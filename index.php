<?php

require __DIR__ . '/vendor/autoload.php';

use App\Logic\Parser;

$parser = new Parser();
echo 'Result: ' . $parser->parse($argv[1]);