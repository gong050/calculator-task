<?php

namespace Tests\Unit;

use App\Logic\Parser;
use Exception;
use PHPUnit\Framework\TestCase;

class ParserTest extends TestCase
{
    /**
     * Test valid data.
     *
     * @dataProvider validDataProvider
     *
     * @param string $string
     * @param float  $expected
     *
     * @throws Exception
     */
    public function testValidData(string $string, float $expected): void
    {
        $parser = new Parser();
        $result = $parser->parse($string);

        $this->assertEquals($result, $expected);
    }

    /**
     * Test invalid data.
     *
     * @dataProvider invalidDataProvider
     *
     * @param string $string
     */
    public function testInvalidData(string $string): void
    {
        $parser = new Parser();
        $this->expectException(Exception::class);
        $parser->parse($string);
    }

    /**
     * Valid data provider.
     *
     * @return array|array[]
     */
    public function validDataProvider(): array
    {
        return [
            ['1+1', 2],
            ['2+2*2', 6],
            ['4*(10-2)/2', 16],
            ['2^(2+2)*3', 48],
        ];
    }

    /**
     * Invalid data provider
     *
     * @return array|\string[][]
     */
    public function invalidDataProvider(): array
    {
        return [
            ['(1+1'],
            ['1+1)'],
            ['()1+2)'],
            ['2asd02+2'],
            ['a1'],
        ];
    }
}
