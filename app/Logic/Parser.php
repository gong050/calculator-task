<?php

declare(strict_types=1);

namespace App\Logic;

use App\Enums\Lexem;
use Exception;

class Parser
{
    /** @var Lexem Токен */
    private Lexem $token;

    /** @var int  Индекс символа */
    private int $index = 0;

    /** @var int Счетчик скобок */
    private int $bracketCount = 0;

    /**
     * Функция парсера
     *
     * @param string $input
     *
     * @return float
     * @throws Exception
     */
    public function parse(string $input): float
    {
        $this->token = Lexer::scan($input, $this->index);
        $result = $this->parseE($input);

        if($this->token == Lexem::R_BRACKET() || $this->bracketCount !== 0) {
            throw new Exception('Проблемы со скобками');
        }

        return $result;
    }

    /**
     * E -> E+T | E-T | T
     *
     * @param string $input
     *
     * @return float
     * @throws Exception
     */
    private function parseE(string $input): float
    {
        if ($this->token == Lexem::NUMBER() || $this->token == Lexem::L_BRACKET()) {
            $result = $this->parseT($input);

            if ($this->token == Lexem::PLUS()) {
                while ($this->token == Lexem::PLUS() && $this->token != Lexem::R_BRACKET()) {
                    $this->token = Lexer::scan($input, $this->index);

                    if ($this->token == Lexem::NUMBER() || $this->token == Lexem::L_BRACKET()) {
                        $resultT = $this->parseT($input);
                        $result += $resultT;
                    } else throw new \Exception('Ошибка в символе: ' . $input[$this->index] . ', индекс: ' . $this->index);
                }

                if ($this->token == Lexem::R_BRACKET()) {
                    $this->token = Lexer::scan($input, $this->index);
                    $this->bracketCount -= 1;
                }
            }

            if ($this->token == Lexem::MINUS()) {
                while ($this->token == Lexem::MINUS() && $this->token != Lexem::R_BRACKET()) {
                    $this->token = Lexer::scan($input, $this->index);

                    if ($this->token == Lexem::NUMBER() || $this->token == Lexem::L_BRACKET()) {
                        $resultT = $this->parseT($input);
                        $result -= $resultT;
                    } else throw new \Exception('Ошибка в символе: ' . $input[$this->index] . ', индекс: ' . $this->index);
                }

                if ($this->token == Lexem::R_BRACKET()) {
                    $this->token = Lexer::scan($input, $this->index);
                    $this->bracketCount -= 1;
                }
            }

            return $result;
        }

        return -1;
    }

    /**
     * T -> T*F | T/F | F
     *
     * @param string $input
     *
     * @return float
     * @throws Exception
     */
    private function parseT(string $input): float
    {
        if ($this->token == Lexem::NUMBER() || $this->token == Lexem::L_BRACKET()) {
            $result = $this->parseF($input);

            if ($this->token == Lexem::MULTIPLY()) {
                while ($this->token == Lexem::MULTIPLY() && $this->token != Lexem::R_BRACKET()) {
                    $this->token = Lexer::scan($input, $this->index);

                    if ($this->token == Lexem::NUMBER() || $this->token == Lexem::L_BRACKET()) {
                        $resultF = $this->parseF($input);
                        $result *= $resultF;
                    } else throw new \Exception('Ошибка в символе: ' . $input[$this->index] . ', индекс: ' . $this->index);
                }

                if ($this->token == Lexem::R_BRACKET()) {
                    $this->token = Lexer::scan($input, $this->index);
                    $this->bracketCount -= 1;
                }
            }

            if ($this->token == Lexem::DIVISION()) {
                while ($this->token == Lexem::DIVISION() && $this->token != Lexem::R_BRACKET()) {
                    $this->token = Lexer::scan($input, $this->index);

                    if ($this->token == Lexem::NUMBER() || $this->token == Lexem::L_BRACKET()) {
                        $resultF = $this->parseF($input);
                        $result /= $resultF;
                    } else throw new \Exception('Ошибка в символе: ' . $input[$this->index] . ', индекс: ' . $this->index);
                }

                if ($this->token == Lexem::R_BRACKET()) {
                    $this->token = Lexer::scan($input, $this->index);
                    $this->bracketCount -= 1;
                }
            }

            return $result;
        }

        return -1;
    }

    /**
     * F -> F^B | B
     *
     * @param string $input
     *
     * @return float
     * @throws Exception
     */
    private function parseF(string $input): float
    {
        if ($this->token == Lexem::NUMBER() || $this->token == Lexem::L_BRACKET()) {
            $result = $this->parseB($input);

            if ($this->token == Lexem::POWER()) {
                while ($this->token == Lexem::POWER() && $this->token != Lexem::R_BRACKET()) {
                    $this->token = Lexer::scan($input, $this->index);

                    if ($this->token == Lexem::NUMBER() || $this->token == Lexem::L_BRACKET()) {
                        $resultB = $this->parseB($input);
                        $result = pow($result, $resultB);
                    } else throw new Exception('Ошибка в символе: ' . $input[$this->index] . ', индекс: ' . $this->index);
                }

                if ($this->token == Lexem::R_BRACKET()) {
                    $this->token = Lexer::scan($input, $this->index);
                    $this->bracketCount -= 1;
                }
            }

            return $result;
        }

        return -1;
    }

    /**
     * B -> int | (E)
     *
     * @param string $input
     *
     * @return float
     * @throws Exception
     */
    private function parseB(string $input): float
    {
        if ($this->token == Lexem::NUMBER()) {
            $number = Lexem::$number;
            $this->token = Lexer::scan($input, $this->index);

            return $number;
        }

        if ($this->token == Lexem::L_BRACKET()) {
            $this->bracketCount += 1;
            $this->token = Lexer::scan($input, $this->index);

            return $this->parseE($input);
        }
    }
}