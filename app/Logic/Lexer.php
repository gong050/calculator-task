<?php

declare(strict_types=1);

namespace App\Logic;

use App\Enums\Lexem;
use Exception;

/**
 * Класс Lexer необходим для лексического анализа входящей строки.
 *
 * @package App\Logic
 */
class Lexer
{
    /**
     * Сканнер лексем.
     *
     * @param string $string
     * @param int    $index
     *
     * @return Lexem
     * @throws Exception
     */
    public static function scan(string $string, int &$index): Lexem
    {
        if (strlen($string) == $index) {
            return Lexem::EXIT();
        }

        switch ($string[$index]) {
            case Lexem::PLUS()->getValue():
                $index++;
                return Lexem::PLUS();

            case Lexem::MINUS()->getValue():
                $index++;
                return Lexem::MINUS();

            case Lexem::DIVISION()->getValue():
                $index++;
                return Lexem::DIVISION();

            case Lexem::MULTIPLY()->getValue():
                $index++;
                return Lexem::MULTIPLY();

            case Lexem::POWER()->getValue():
                $index++;
                return Lexem::POWER();

            case Lexem::L_BRACKET()->getValue():
                $index++;
                return Lexem::L_BRACKET();

            case Lexem::R_BRACKET()->getValue():
                $index++;
                return Lexem::R_BRACKET();

            case is_numeric($string[$index]):
                Lexem::$number = self::getNumberFromString($string, $index);
                return Lexem::NUMBER();

            default:
                throw new Exception('Введен неверный символ');
        }
    }

    /**
     * Вычисление числа из строки.
     *
     * @param string $string
     * @param int    $index
     *
     * @return int
     */
    public static function getNumberFromString(string $string, int &$index): int
    {
        $result = '';

        while(is_numeric($string[$index])) {
            $result .= $string[$index];
            $index++;

            if (strlen($string) == $index) {
                break;
            }
        }

        return (int) $result;
    }
}