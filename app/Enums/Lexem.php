<?php

declare(strict_types=1);

namespace App\Enums;

use MyCLabs\Enum\Enum;

/**
 * Класс Lexem enum лексем.
 *
 * @method static PLUS()
 * @method static MINUS()
 * @method static DIVISION()
 * @method static POWER()
 * @method static MULTIPLY()
 * @method static L_BRACKET()
 * @method static R_BRACKET()
 * @method static NUMBER()
 * @method static EXIT()
 */
class Lexem extends Enum
{
    private const PLUS = '+';
    private const MINUS = '-';
    private const DIVISION = '/';
    private const MULTIPLY = '*';
    private const POWER = '^';
    private const L_BRACKET = '(';
    private const R_BRACKET = ')';
    private const NUMBER = 'NUMBER';
    private const EXIT = 'EXIT';
    public static int $number;
}